# Copyright 2009 Nestor Ovroy <novroy@riseup.net>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'polipo-1.0.4.ebuild' which is
#     Copyright 1999-2009 Gentoo Foundation

require systemd-service

SUMMARY="A web caching proxy"
DESCRIPTION="
Polipo is a small and fast caching web proxy (a web cache, an HTTP proxy, a
proxy server). While Polipo was designed to be used by one person or a small
group of people, there is nothing that prevents it from being used by a larger
group.
"
HOMEPAGE="http://www.pps.jussieu.fr/~jch/software/polipo/"
DOWNLOADS="http://freehaven.net/~chrisd/polipo/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        group/polipo
        user/polipo
"

DEFAULT_SRC_COMPILE_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    CDEBUGFLAGS="${CFLAGS}"
)

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    MANDIR=/usr/share/man
    INFODIR=/usr/share/info
    TARGET="${IMAGE}"
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=(
    config.sample
)

src_install() {
    default

    insinto /etc/polipo
    newins config.sample config

    install_systemd_files

    # Clean the cache daily
    exeinto /etc/cron.daily
    newexe "${FILES}"/polipo.crond polipo

    diropts -m750 -o polipo -g polipo
    keepdir /var/cache/polipo
}

pkg_postinst() {
    elog "Edit /etc/polipo/config before initial use of Polipo. The following"
    elog "configuration is recommended when using Polipo with Tor:"
    elog "    https://gitweb.torproject.org/torbrowser.git/blob_plain/HEAD:/build-scripts/config/polipo.conf"
}

