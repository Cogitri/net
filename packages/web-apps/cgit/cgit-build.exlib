# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2012-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_{prepare,test,install} pkg_postinst

# grep "^GIT_VER =" Makefile
myexparam git_version
git_version=$(exparam git_version)

SUMMARY="A fast webinterface for git"
DESCRIPTION="
cgit is a cgi application implemented in C.
* all git operations are performed by linking with libgit
* uses a built-in cache on disk
* fond of virtual urls, using PATH_INFO or modules like mod_rewrite makes cgit
  generate urls with few or no querystring parameters
"
HOMEPAGE="https://git.zx2c4.com/${PN}"
DOWNLOADS="
    ${HOMEPAGE}/snapshot/${PNV}.tar.xz
    mirror://kernel/software/scm/git/git-${git_version}.tar.xz
"

BUGS_TO="philantrop@exherbo.org"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
    build+run:
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    test:
        app-text/tidy
"

CGIT_INSTALL_DIR=/usr/$(exhost --target)/lib/${PN}

GIT_BUILD_OPTIONS=(
    AR=$(exhost --tool-prefix)ar
    CC=$(exhost --tool-prefix)cc
    PKG_CONFIG=$(exhost --tool-prefix)pkg-config
    NO_LUA=1
)

DEFAULT_SRC_COMPILE_PARAMS=(
    NO_LUA=1
    GIT_OPTIONS="${GIT_BUILD_OPTIONS[@]}"
    all doc-man
)

DEFAULT_SRC_INSTALL_PARAMS=(
    CGIT_SCRIPT_PATH="${CGIT_INSTALL_DIR}"
    NO_LUA=1
    "${GIT_BUILD_OPTIONS[@]}"
    install-man
)

cgit-build_src_prepare() {
    default

    # Make sure the output is verbose during every stage.
    export V=1

    # Remove the bundled git version...
    edo rmdir "${WORK}"/git

    # ... and instead use the downloaded copy.
    edo mv "${WORK}"/../git-"${git_version}" "${WORK}"/git

    # Fix LIBDIR.
    edo sed -i -e "/libdir =/s:/lib:/$(exhost --target)/lib:" Makefile

    # Fix prefix.
    edo sed -i -e "/prefix =/s:/usr/local:/usr:" Makefile
    edo sed -i -e "/^CC =/s:cc:$(exhost --tool-prefix)&:" git/Makefile
    edo sed -i -e "/^AR =/s:ar:$(exhost --tool-prefix)&:" git/Makefile
}

cgit-build_src_test() {
    # This test is broken.
    edo rm tests/t0109-gitconfig.sh

    emake NO_LUA=1 CC=${CC} -j1 test
}

cgit-build_src_install() {
    default

    keepdir /var/cache/cgit

    dodir /var/lib/cgit/filters
    insinto /var/lib/cgit/filters/
    doins "${WORK}"/filters/*
}

cgit-build_pkg_postinst() {
    default

    elog "The cgit executable and accompanying stuff have been installed into"
    elog "${CGIT_INSTALL_DIR}. You will have to copy/link it to the intended directory"
    elog "yourself. This usually is somewhere below /srv/www on Exherbo."
}

